#!/bin/sh

set -e

apt-get install -y opam ca-certificates gnupg libipc-system-simple-perl libstring-shellquote-perl
apt-get install -y $(cat "$(dirname $0)/debs-to-install.txt")

useradd -s /bin/bash builder
mkdir /home/builder
chown builder:builder /home/builder
chown builder:builder -R "$(dirname $0)"
