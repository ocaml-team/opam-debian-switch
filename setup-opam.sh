#!/bin/sh

set -e

rm -f opam-installed.txt opam-repo.txt debian-repo.txt
grep "^Date:" /var/lib/apt/lists/*_unstable_InRelease > debian-repo.txt
opam init --bare --disable-sandboxing --no-setup
opam switch create 5.3.0
cp /home/builder/.opam/repo/default/repo opam-repo.txt
eval $(opam env)
opam install --yes --with-test $(cat "$(dirname $0)/opams-to-install-test.txt")
opam install --yes $(cat "$(dirname $0)/opams-to-install-notest.txt")
opam list --short --columns=name,version > opam-installed.txt
