#!/usr/bin/env ocaml

#use "topfind";;
#require "opam-file-format";;

let file = Sys.argv.(1);;

let f = OpamParser.FullPos.file file;;

let url =
  let open OpamParserTypes.FullPos in
  List.find_map
    (fun x ->
      match x.pelem with
      | Section x when x.section_kind.pelem = "url" ->
         List.find_map
           (fun x ->
             match x.pelem with
             | Variable ({pelem = ("src" | "archive"); _}, {pelem = String x; _}) -> Some x
             | _ -> None
           ) x.section_items.pelem
      | _ -> None
    ) f.file_contents
;;

let () = Option.iter print_endline url;;
