Comparing Debian packages with opam packages
============================================

This repository helps in comparing Debian packages with opam
packages. The approach is to create, in a chroot, an opam switch with
all packages present in Debian. Then, you can for example compare
versions installed by opam, or try to perform updates.

Workflow
--------

 * run `make`
 * create a disposable chroot and open a shell in it (thanks to
   namespaces, you don't need to be root):
```
TMPDIR=/path/to/fs/big/enough mmdebstrap --variant=apt unstable \
  --customize-hook='chroot "$1" mkdir /tmp/ods' \
  --customize-hook='copy-in . /tmp/ods' \
  --customize-hook='chroot "$1" /tmp/ods/setup-chroot.sh' \
  --customize-hook='chroot "$1" bash' \
  /dev/null http://deb.debian.org/debian
```
 * in this chrooted shell:
   + go to `/tmp/ods`
   + run `runuser -u builder ./setup-opam.sh` and debug it until
     success
 * copy `debian-repo.txt`, `opam-repo.txt` and `opam-installed.txt`
   from the `/tmp/ods` in the chroot to your working directory outside
   the chroot
 * run `make`
 * analyze the differences with `git diff`
 * commit if relevant

It takes approximately 20 min and 20 GB.

How it works
------------

The file `pkgmap.json` maps Debian source packages to opam
packages. One Debian package can install the contents of several opam
packages, so the mapping is to lists. When a list is empty, its means
that the corresponding Debian package is not available in
opam. Sometimes, the package exists in Debian and opam, but the opam
one is uninstallable (due to dependency-type constraints): this is
marked with a leading `!` in the opam package name. Sometimes, an opam
package is declared installable, but fails to install (due to a
build-time failure): this is marked with a leading `%` in the opam
package name. Sometimes, an opam package exists because of the way the
software is packaged in opam, but the existence of the package is not
relevant to Debian: this is marked with a leading `#` in the opam
package name. Sometimes, an opam package cannot be installed with
tests enabled: this is marked with a leading `@` in the opam package
name.

Packages with a leading `!`, `%` or `#` are ignored when installing
packages.

The file `debdeps.json` maps opam packages to their Debian
dependencies. It typically applies to `conf-*` packages.

The file `opams-to-install.txt` lists all opam packages available in
Debian; the workflow will install them all in the chroot, after having
installed the Debian packages listed in `debs-to-install.txt`.

The file `opam-installed.txt` is the list of installed packages (and
their version) reported by opam in the chroot.

The script `update.ml`:
 * looks up interesting Debian packages in unstable/main
 * generates `debs-to-install.txt` and `opams-to-install.txt`
 * loads `opam-installed.txt`
 * generates `newer-in-opam.txt` and `newer-in-debian.txt`

It embeds a list of opam packages to ignore for computing
up-to-dateness, either because their version is meaningless (typically
base or transition packages) or they are installed by a Debian package
which has a different versioning scheme (this happens when two opam
packages with the same tarball have different versions).

Checking that upstream tarballs are the same as in opam
-------------------------------------------------------

The `./check-upstreams.sh` script downloads and compares upstream
tarballs in Debian and opam. It expects the [opam
repository](https://github.com/ocaml/opam-repository/) in the
directory `opam-repository`.
