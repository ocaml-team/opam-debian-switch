#!/usr/bin/env ocaml

#use "topfind";;
#thread;;
#require "yojson";;
#require "ben";;
#require "ben.frontends";;

open Ben
open Core

let list_of_json x =
  match x with
  | `List x ->
     List.map
       (function
        | `String x -> x
        | _ -> invalid_arg "`String expected"
       ) x
  | _ -> invalid_arg "`List expected"

let map_of_json x =
  match x with
  | `Assoc x ->
     List.fold_left
       (fun accu (deb, opam)->
         StringMap.add deb (list_of_json opam) accu
       ) StringMap.empty x
  | _ -> invalid_arg "`Assoc expected"

let pkgmap =
  Yojson.Safe.from_file "pkgmap.json"
  |> map_of_json

let debdeps =
  Yojson.Safe.from_file "debdeps.json"
  |> map_of_json

let write_to_file filename list =
  with_out_file filename
    (fun oc -> List.iter (fun x -> Printf.fprintf oc "%s\n" x) list)

let opams_to_install_all =
  StringMap.fold
    (fun _ opkgs accu ->
      List.rev_append opkgs accu
    ) pkgmap []
  |> List.filter (fun x -> not @@ starts_with x "!")
  |> List.filter (fun x -> not @@ starts_with x "%")
  |> List.filter (fun x -> not @@ starts_with x "#")
  |> List.filter (fun x -> not @@ starts_with x "conf-")
  |> List.sort compare |> uniq

let opams_to_install_notest, opams_to_install_test =
  opams_to_install_all
  |> List.partition (fun x -> starts_with x "@")

let opams_to_install_notest =
  opams_to_install_notest
  |> List.map (fun x -> String.sub x 1 (String.length x - 1))

let debs_to_install =
  StringMap.fold
    (fun _ opkgs accu ->
      List.fold_left
        (fun accu opkg ->
          let dpkgs =
            match StringMap.find_opt opkg debdeps with
            | Some x -> x
            | None -> []
          in
          List.rev_append dpkgs accu
        ) accu opkgs
    ) pkgmap []
  |> List.sort compare |> uniq

let () = write_to_file "opams-to-install-test.txt" opams_to_install_test
let () = write_to_file "opams-to-install-notest.txt" opams_to_install_notest
let () = write_to_file "debs-to-install.txt" debs_to_install

let remove_prefix ~prefix x =
  let p = String.length prefix and n = String.length x in
  if n >= p && String.sub x 0 p = prefix then
    String.sub x p (n - p)
  else
    x

let reverse_pkgmap =
  StringMap.fold
    (fun deb opam accu ->
      List.fold_left
        (fun accu opam ->
          let opam = remove_prefix ~prefix:"#" opam in
          let opam = remove_prefix ~prefix:"@" opam in
          StringMap.add opam deb accu
        ) accu opam
    ) pkgmap StringMap.empty

let pkgmap_ignore =
  StringMap.fold
    (fun _ opam accu ->
      List.fold_left
        (fun accu opam ->
          if String.starts_with ~prefix:"#" opam then
            remove_prefix ~prefix:"#" opam :: accu
          else accu
        ) accu opam
    ) pkgmap []

let query = Query.of_string ".build-depends ~ /ocaml/ | .build-depends-indep ~ /ocaml/"

let query_sources filename =
  Ben_frontends.Utils.parse_control_file Source filename
    (fun x -> true)
    (fun name pkg accu ->
      if Query.eval_source pkg query then
        match Package.Map.find_opt name accu with
        | None -> Package.Map.add name pkg accu
        | Some pkg' ->
           let version = Package.get "version" pkg and
               version' = Package.get "version" pkg'
           in
           if Debian_version.compare version' version < 0 then
             Package.Map.add name pkg accu
           else
             accu
      else
        accu
    ) Package.Map.empty

let srcs = query_sources Sys.argv.(1)

let parse_opam_line =
  let open Re.Pcre in
  let rex = regexp "^(\\S+)\\s+(\\S+)$" in
  fun line ->
  match exec ~rex line with
  | s -> Some (get_substring s 1, get_substring s 2)
  | exception Not_found -> None

let parse_opam_installed filename =
  with_in_file filename
    (fun ic ->
      let rec loop lines =
        match input_line ic with
        | line -> loop (line :: lines)
        | exception End_of_file -> lines
      in
      loop []
    )
  |> List.fold_left
       (fun accu line ->
         match parse_opam_line line with
         | None -> accu
         | Some (name, version) -> StringMap.add name version accu
       ) StringMap.empty

let opam_installed = parse_opam_installed "opam-installed.txt"

let not_in_debian =
  StringMap.fold
    (fun opkg version accu ->
      if StringMap.mem opkg reverse_pkgmap then
        accu
      else
        StringMap.add opkg version accu
    ) opam_installed StringMap.empty
  |> StringMap.bindings

let find_in_pkgmap src =
  match StringMap.find src pkgmap with
  | x -> x
  | exception Not_found ->
     if
       String.starts_with ~prefix:"coq" src
       || String.starts_with ~prefix:"mathcomp-" src
     then [] else raise Not_found

let not_in_opam =
  Package.Map.fold
    (fun src pkg accu ->
      let src = Package.Name.to_string src in
      match find_in_pkgmap src with
      | [] -> StringMap.add src (Package.get "version" pkg) accu
      | _ -> accu
      | exception Not_found -> Printf.ksprintf failwith "missing in pkgmap: %s" src
    ) srcs StringMap.empty
  |> StringMap.bindings

let ignore_list =
  [
    "host-arch-x86_64";
    "host-system-other";
    "mirage-no-solo5";
    "mirage-no-xen";
  ] @ pkgmap_ignore

let demangle_upstream_version =
  let open Re.Pcre in
  let rexs =
    [
      regexp {|^(.+)(?:[.+](?:deb|dfsg|official|debian|ds)[0-9]*)$|};
      regexp {|^(?:0~)(.+)$|};
    ]
  in
  fun version ->
  List.fold_left (fun version rex ->
      match exec ~rex version with
      | s -> get_substring s 1
      | exception Not_found -> version) version rexs

let rec assoc_default key = function
  | [] -> raise Not_found
  | [_, x] -> x
  | (key', x) :: _ when key = key' -> x
  | _ :: xs -> assoc_default key xs

let deb_extract_upstream_version =
  let open Re.Pcre in
  let rexs =
    [
      "ocaml-getopt", regexp {|^0\.0\.(\d+)-[^-]+$|};
      "_", regexp "^(?:[0-9]+:)?(.+)(-[^-]+)$";
    ]
  in
  fun ~dpkg version ->
  let rex = assoc_default dpkg rexs in
  match exec ~rex version with
  | s -> demangle_upstream_version (get_substring s 1)
  | exception Not_found -> version

let opam_extract_upstream_version =
  let open Re.Pcre in
  let rexs =
    [
      "reactiveData", regexp {|^(.+)\.\d+$|};
      "_", regexp "^(.+)(-[^-]+)$";
    ]
  in
  fun ~opkg version ->
  let rex = assoc_default opkg rexs in
  match exec ~rex version with
  | s -> get_substring s 1
  | exception Not_found -> version

let fixup_opam_version =
  let open Re.Pcre in
  let rex = regexp "^v?(.+)$" in
  fun version ->
  match exec ~rex version with
  | s -> get_substring s 1
  | exception Not_found -> version

let get_version pkg =
  match Package.(get "version" (Map.find (Name.of_string pkg) srcs)) with
  | x -> x
  | exception Not_found ->
     Printf.ksprintf failwith "Unable to find version of %s" pkg

let newer_in_debian, newer_in_opam, in_sync =
  StringMap.fold
    (fun opkg oversion ((newer_in_debian, newer_in_opam, in_sync) as accu) ->
      let oversion = fixup_opam_version oversion in
      if starts_with opkg "conf-" || List.mem opkg ignore_list then
        accu
      else
        match StringMap.find_opt opkg reverse_pkgmap with
        | Some dpkg ->
           let dversion = get_version dpkg in
           let ouversion = opam_extract_upstream_version ~opkg oversion in
           let duversion = deb_extract_upstream_version ~dpkg dversion in
           let cmp = Debian_version.compare ouversion duversion in
           if cmp < 0 then
             (((dpkg, duversion), (opkg, ouversion)) :: newer_in_debian, newer_in_opam, in_sync)
           else if cmp > 0 then
             (newer_in_debian, ((dpkg, duversion), (opkg, ouversion)) :: newer_in_opam, in_sync)
           else
             (newer_in_debian, newer_in_opam, (opkg, oversion, dpkg, dversion) :: in_sync)
        | None -> accu
    ) opam_installed ([], [], [])

let merge_opam_packages pkgs =
  List.fold_left
    (fun accu ((dpkg, dversion), (opkg, oversion)) ->
      match StringMap.find_opt dpkg accu with
      | Some (dversion', oversion') ->
         if dversion = dversion' then
           if oversion = oversion' then
             accu
           else
             Printf.ksprintf failwith
               "multiple opam versions for %s (at least %s and %s)"
               dpkg oversion oversion'
         else
           Printf.ksprintf failwith
             "multiple Debian versions for %s (at least %s and %s)"
             dpkg dversion dversion'
      | None -> StringMap.add dpkg (dversion, oversion) accu
    ) StringMap.empty pkgs
  |> StringMap.bindings

let () =
  merge_opam_packages newer_in_opam
  |> List.map
       (fun (dpkg, (dversion, oversion)) ->
         Printf.sprintf "%s: %s -> %s" dpkg dversion oversion
       )
  |> List.merge compare
       (not_in_debian
        |> List.map
            (fun (opkg, version) ->
              Printf.sprintf "%s: null -> %s" opkg version
            )
       )
  |> write_to_file "newer-in-opam.txt"

let () =
  merge_opam_packages newer_in_debian
  |> List.map
       (fun (dpkg, (dversion, oversion)) ->
         Printf.sprintf "%s: %s <- %s" dpkg dversion oversion
       )
  |> List.merge compare
       (not_in_opam
        |> List.map
             (fun (dpkg, version) ->
               Printf.sprintf "%s: %s <- null" dpkg version
             )
       )
  |> write_to_file "newer-in-debian.txt"

let () =
  List.sort compare in_sync
  |> List.map
       (fun (opkg, oversion, dpkg, dversion) ->
         Printf.sprintf "%s/%s ~ %s/%s" opkg oversion dpkg dversion
       )
  |> write_to_file "in-sync.txt"
