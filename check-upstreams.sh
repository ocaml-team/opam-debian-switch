#!/bin/sh

ods=$(pwd)
wdir=$(mktemp -d /tmp/check-upstreams.XXXXXX)
log=check-upstreams.log

echo "Working directory is $wdir"

cd $wdir

while read opam x deb; do
    echo "===> $opam $x $deb <===" >> $log
    opkg=${opam%/*}
    oversion=${opam#*/}
    dpkg=${deb%/*}
    dversion=${deb#*/}
    opam_url=$( ( cd $ods && ./opam-get-upstream.ml opam-repository/packages/$opkg/$opkg.*$oversion/opam ) )
    if [ -n "$opam_url" ]; then
        mkdir -p $dpkg
        ( cd $dpkg && apt-get source -d --only-source $dpkg=$dversion ) >> $log 2>&1
        upstream_file_debian=$(ls $dpkg/${dpkg}_*.orig.tar.* | grep -v '\.asc$')
        upstream_file_opam=$dpkg/${opam_url##*/}
        ( cd $dpkg && wget $opam_url ) >> $log 2>&1
        if cmp -s $upstream_file_opam $upstream_file_debian; then
            cmp="="
        else
            if [ "${upstream_file_opam%.zip}" = "$upstream_file_opam" ]; then
                extract="tar -xf"
            else
                extract="unzip -q"
            fi
            ( mkdir -p $dpkg/debian && cd $dpkg/debian && tar -xf ../../$upstream_file_debian )
            ( mkdir -p $dpkg/opam && cd $dpkg/opam && $extract ../../$upstream_file_opam )
            diff -ur $dpkg/debian/* $dpkg/opam/* > $dpkg/diff
            cmp="<>"
        fi
        echo "$opam $cmp $deb"
    else
        echo "$opam missing"
    fi
done < $ods/in-sync.txt

exit 0
